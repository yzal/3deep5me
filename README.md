This is the development repository for the 3Deep5Me Meme Machine.

Here you will find the custom parts used.

Recent Changes:

- Added Marlin firmware files (using latest RCBugFix as of March 2nd, 2017)
- Updated GCode Parser to reduce stringing and prevent the pen from knocking the print over
- Added precompiled GCode Parser binary (Windows only)

Filament Container:

- Uses the filament stand by [jjpowelly](https://goo.gl/lx11wj) with a modified bar to accommodate smaller filament rolls. 


Parts:

- Extruder:
	- Uses the [Prusa i3 Rework geared extruder](https://github.com/eMotion-Tech/Prusai3_EINSTEIN_Reworked/blob/master/Pieces%20en%20STL/EXTRUDEUR/BODY-EXTRUDEUR-WADE/BODY-EXTRUDEUR-WADE.stl)
	- The extruder fan has been relocated to mount on one of the unused screw holes of the X axis carriage. The unused mount is used to hold the adjustable print cooling fan.
		- The fan mount is currently custom designed, it will eventually be a parametric part.
	- The house shaped hole on the side of the extruder is used to mount the pen holder for colored prints (WIP)

- PSU:
	- Currently custom designed for one specific power supply, a parametric design will be uploaded later

- X Axis:
	- Based on the belt tensioner by [kennethjiang](https://goo.gl/QO8vX2), adapted to work with MK2 parts.
	- The endstop mount can be mounted anywhere along the axis, useful for smaller build surfaces.
		- Only one half of the mount is provided, mirror it in your slicer to print the whole thing.
	- The motor and axis mounts provided have rounded bottoms if you wish to home your printer below the endstop for some reason. The original mounts from the Rework will work fine as well.
		- If you choose to make the printer home below the endstops, you will have to design your own endstop mount for the Z Axis

- Y Axis: 
	- The endstop mount relies on the Y Motor mount from the [Prusa i3 Rework](https://github.com/eMotion-Tech/Prusai3_EINSTEIN_Reworked/blob/master/Pieces%20en%20STL/Y-MOTOR/Y-MOTOR.stl)

- Z Axis:
	- The top mount has an extra hole added to guide the M5 threaded rod and reduce wobble
	- The endstop mounts must be drilled into the side of the frame,
	- Only one side of the mount has been provided, you must print a second one mirrored or only use one endstop.

GCode Parser:

- In order to color the print, GCode from a slicer must be modified to make the head trace each layer at an offset.
	- Currently supports Craftware and Cura
	- NEW: Rudimentary layer selection is now avalible
	- NEW: Can choose both perimeter and solid color.