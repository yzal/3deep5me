#include "ParserTools.h"

int countlines(FILE* fp) {
	int lines = 1;
	char check;

	while (!feof(fp)) {
		check = fgetc(fp);
		if (check == '\n')
			lines++;
	}

	return lines;
}

long long int findchar(FILE* fp, char ch, int order) {
	long long int index;
	char check;

	order++;

	while ((!feof(fp)) && order) {
		check = fgetc(fp);
		if (check == ch)
			order--;
	}

	index = ftell(fp);

	RESET(fp);

	if (order)
		return -1;

	return index;
}

void append(char* str, char ch) {
	int len = strlen(str);
	str[len] = ch;
	str[len + 1] = 0;
}

int compstring(char* check, char* against) {
	int len = strlen(against);
	for (int i = 0; i < len; i++) {
		if ((check[i] != against[i]) && check[i] != 0)
			return 0;
		else if (check[i] == 0)
			return -1;
	}
	return 1;
}

long long int findstr(FILE* fp, char* str, int order) {
	long long int index;
	int len = strlen(str);
	char ch;
	char* check = (char*)malloc((len + 1)*sizeof(char));
	check[0] = 0;

	order++;

	while ((!feof(fp)) && order) {
		ch = fgetc(fp);
		append(check, ch);
		int samechars = compstring(check, str);
		if (!samechars) {
			check[0] = 0;
		}
		else if (samechars == 1){
			order--;
			check[0] = 0;
		}
	}

	index = ftell(fp) - len;

	RESET(fp);

	if (order)
		return -1;

	return index;
}

FILE* allcaps(FILE* fp) {
	FILE* ac = fopen("allcaps.gcode", "w+");

	char buffer;

	if (ac == NULL) {
		printf("Could not write to file\n");

		system("PAUSE");
		exit(0);
	}

	while (!feof(fp)) {
		buffer = fgetc(fp);
		buffer = toupper(buffer);
		fputc(buffer, ac);
	}

	return ac;
}
