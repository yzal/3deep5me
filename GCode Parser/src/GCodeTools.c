#include "GCodeTools.h"

void loadSettings(void){
	FILE* settings = fopen(SETTINGS, "r");

	if (settings == NULL){
		getSettings();
		return;
	}

	int m_num_settings = 0;
	char option[20];
	double value;

	while (!feof(settings)) {
		if (fscanf(settings, "%s %lf", option, &value) != 2){
			printf("Error: %s has been corrupted, please reconfigure.\n", SETTINGS);
			system("pause");
			getSettings();
			return;
		}

		switch (option[0]){
		case 'X':
			m_num_settings++;
			sharpie.x = value;
			break;
		case 'Y':
			m_num_settings++;
			sharpie.y = value;
			break;
		case 'S':
			m_num_settings++;
			sharpie.servo = value;
			break;
		case 'E':
			m_num_settings++;
			sharpie.servo_on = value;
			break;
		case 'D':
			m_num_settings++;
			sharpie.servo_off = value;
			break;
		default:
			printf("Error: %s has been corrupted, please reconfigure.\n", SETTINGS);
			system("pause");
			getSettings();
			return;
		}
	}
}

void getSettings(void){
	FILE* settings = fopen(SETTINGS, "w");

	if (settings == NULL){
		printf("Error: Could not open %s for writing, values will not be saved on next run.\n", SETTINGS);
	}

	printf("Please enter the X offset of your pen: ");
	scanf("%lf", &sharpie.x);
	printf("Please enter the Y offset of your pen: ");
	scanf("%lf", &sharpie.y);
	printf("Please enter the servo number for your pen: ");
	scanf("%d", &sharpie.servo);
	printf("Please enter the servo position when the pen is active: ");
	scanf("%d", &sharpie.servo_on);
	printf("Please enter the servo position when the pen is deactivated: ");
	scanf("%d", &sharpie.servo_off);

	fprintf(settings, "X: %f\n", sharpie.x);
	fprintf(settings, "Y: %f\n", sharpie.y);
	fprintf(settings, "Servo: %d\n", sharpie.servo);
	fprintf(settings, "Enable: %d\n", sharpie.servo_on);
	fprintf(settings, "Disable: %d", sharpie.servo_off);

	fclose(settings);

}

void showMenu(void){
	printf("Select an option:\n");
	printf("0. Reconfigure offset settings\n");
	printf("1. Cura\n");
	printf("2. Craftware\n");
	printf("\n");
}

void setSlicer(void){

	slicer = getc(stdin) - 48;
	//Clear stdin
	while (getc(stdin) != '\n');

	switch (slicer){
	case 0:
		getSettings();
		break;
	case CURA:
		sprintf(perim, ";WALL-OUTER");
		sprintf(layer, ";LAYER:");
		sprintf(last_line, ";End of Gcode");
		l_seekback = 0;
		e_seekback = 100;
		break;
	case CRAFTWARE:
		sprintf(perim, ";segType:Perimeter");
		sprintf(layer, "; Layer #");
		sprintf(last_line, ";CraftWare_Settings=");
		l_seekback = 40;
		e_seekback = 45;
		break;
	default:
		printf("Please select a valid option (0-%d): ",CRAFTWARE);
		setSlicer();
		break;
	}
}

void setLayerDelay(void){
	printf("Delay between layers (ms)? ");
	scanf("%d", &wait);
}

void findMaxSeek(FILE* in){
	long long int end = findstr(in, last_line, 0);
	if(end == -1){
		printf("Error: Could not find last line, exiting to prevent seg fault...\n");
		system("pause");
		exit(1);
	}
	max_seek = end;
}


void injectString(FILE* out, char* string){
	fputs(string, out);
}

void injectPenState(FILE* out, bool enable){
	char buffer[20];
	injectString(out, ";Clear GCode buffer\n");
	injectString(out, "M400\n");

	sprintf(buffer, ";Wait for %dms\n", wait);
	injectString(out, buffer);
	sprintf(buffer, "G4 P%d\n", wait);
	injectString(out, buffer);

	sprintf(buffer, ";%s pen\n", enable ? "Engage" : "Disengage");
	injectString(out, buffer);
	sprintf(buffer, "M280 P%d S%d\n", sharpie.servo, (enable ? sharpie.servo_on : sharpie.servo_off));
	injectString(out, buffer);
}

void copyHeader(FILE* in, FILE* out){
	RESET(in);
	RESET(out);

	injectPenState(out, false);

	int seek_begin = 0;
	int seek_end = findstr(in, layer, 0) - l_seekback;

	while(ftell(in) < seek_end){
		fputc(fgetc(in),out);
	}
}

int copyLayer(FILE* in, FILE* out, int l_num){
	RESET(in);

	int status = 1;

	char* buffer[20];

	formatLayer(buffer, l_num);
	long long int seek_begin = findstr(in, buffer, 0) - l_seekback;

	formatLayer(buffer, l_num + 1);
	long long int seek_end = findstr(in, buffer, 0) - l_seekback;

	// printf("initial begin: %d, end: %d\n", seek_begin, seek_end);

	if (seek_begin == (-1-l_seekback))
		status = -1;

	if (seek_end == (-1 - l_seekback)){
		status = 0;
		seek_end = max_seek;
	}

//
	// printf("status: %d\n", status);
	// printf("end begin: %d, end: %d\n", seek_begin, seek_end);

	fseek(in, seek_begin, SEEK_SET);

	while(ftell(in) < seek_end){
		fputc(fgetc(in),out);
	}
	return status;
}

void engagePen(FILE* in, FILE* out, int l_num, char* begin, char* end){
	RESET(in);

	bool is_end = false;

	char buffer[100];
	double extruder_pos;

	formatLayer(buffer, l_num);
	long long int seek_begin = findstr(in, buffer, 0) - l_seekback;

	formatLayer(buffer, l_num + 1);
	long long int seek_end = findstr(in, buffer, 0) - l_seekback;

	if (seek_begin == (-1 - l_seekback)){
		printf("engagePen: Could not find seek_begin\n");
		return;
	}
	if (seek_end == (-1 - l_seekback)){
		printf("engagePen: Could not find seek_end\n");
		seek_end = max_seek - 3;
		is_end = true;
	}

	fseek(in, seek_begin, SEEK_SET);

	long long int seek_target_begin = findstr(in, begin, 0) +strlen(begin) +2;
	fseek(in, seek_target_begin+1, SEEK_SET);

	long long int seek_target_end = seek_end;

	if (!is_end){
		seek_target_end = findstr(in, end, 0);

		fseek(in, seek_end - e_seekback, SEEK_SET);
		long long int e_seekto = findchar(in, 'E', 0);
		fseek(in, e_seekto, SEEK_SET);

		fscanf(in, "%s", buffer);
		//buffer[0] = '0';
		extruder_pos = atof(buffer);

		injectString(out, ";Retracting nozzle\n");
		sprintf(buffer, "G0 F%d E%.4f\n", RETRACT_FEED, extruder_pos - RETRACT);
		injectString(out, buffer);
	}

	injectString(out, ";Making first movement to offset\n");
	fseek(in, seek_target_begin, SEEK_SET);


	double coordinate;
	//Copy first line before lowering pen
	injectString(out, "G0");
	while (buffer[0] != 'X'){
		fscanf(in, "%s", buffer);
	}
	buffer[0] = '0';
	coordinate = atof(buffer);
	coordinate += sharpie.x;
	sprintf(buffer, " X%.4f", coordinate);
	injectString(out, buffer);

	fscanf(in, "%s", buffer);
	buffer[0] = '0';
	coordinate = atof(buffer);
	coordinate += sharpie.y;
	sprintf(buffer, " Y%.4f", coordinate);	
	injectString(out, buffer);
	
	injectPenState(out, true);

	while(ftell(in) < seek_target_end){
		fscanf(in, "%s", buffer);

		switch(buffer[0]){
			case 'G':
				injectString(out, "\n");
				break;
			case 'X':
				buffer[0] = '0';
				coordinate = atof(buffer);
				coordinate += sharpie.x;
				sprintf(buffer, " X%.4f", coordinate);
				break;
			case 'Y':
				buffer[0] = '0';
				coordinate = atof(buffer);
				coordinate += sharpie.y;
				sprintf(buffer, " Y%.4f", coordinate);
				break;
			case 'E':
				buffer[0] = 0;
				break;
			case 'F':
				buffer[0] = 0;
				break;
			case ';': //This may not be required
				buffer[0] = 0;
				break;
			default:
				printf("Warning: Unknown string (%s) found at %d\n",buffer, ftell(in));
				buffer[0] = 0;
				break;
		}

		injectString(out, buffer);

	}
	injectString(out, "\n");

	injectPenState(out, false);
	injectString(out, ";End of offset code\n");
}

//void engagePen(FILE* in, FILE* out, int l_num, char* begin, char* end)

void colorPerim(FILE* in, FILE* out, int l_num){
	engagePen(in, out, l_num, perim, ";");
}
void colorLayer(FILE* in, FILE* out, int l_num){
	engagePen(in, out, l_num, layer, layer);
}

void formatLayer(char* buffer, int l_num){
	sprintf(buffer,"%s%d",layer,l_num);
	return;
}
