#ifndef PARSERTOOLS_H
#define PARSERTOOLS_H

#define _CRT_SECURE_NO_WARNINGS

#define RESET(a) fseek(a,0,SEEK_SET)

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>
#include <ctype.h>



int countlines(FILE* fp);

long long int findchar(FILE* fp, char ch, int order);
long long int findstr(FILE* fp, char* str, int order);

FILE* allcaps(FILE* fp);

void append(char* str, char ch);
int compstring(char* check, char* against);

#endif //PARSERTOOLS_H
