#include "FileTools.h"
FILE* openfile(char* mode){
	OPENFILENAME ofn;

	char szFile[100];

	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = NULL;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = ".gcode\0*.gcode\0All files\0*.*\0.txt\0*.TXT\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	GetOpenFileName(&ofn);

	FILE* fp = fopen(ofn.lpstrFile, mode);

	if (fp == NULL) {
		printf("Error: Could not open file\n");
		system("PAUSE");
		exit(0);
	}

	return fp;
}

FILE* initOutFile(void){
	char filename[255];

	printf("Please enter a filename: ");

	scanf("%s", filename);

	if (!isLegalName(filename)){
		printf("Error: That is not a valid filename\n");
		return initOutFile();
	}

	strcat(filename, ".gcode");

	if (fileExists(filename)){
		char yn;
		
		do{
			//Clear stdin
			while (getc(stdin) != '\n');

			printf("Warning that file already exists and will be overwritten, continue? (y/n) ");
			yn = getc(stdin);
		} while (yn != 'y' && yn != 'n');

		if (yn == 'n'){
			return initOutFile();
		}
	}

	return fopen(filename, "w");
}

bool isLegalName(char *name){
	int index = 0;

	while (name[index] != 0){
		if (INVALID_CHARS(name[index]))
			return false;
		index++;
	}
	return true;
}

bool fileExists(char *name){
	FILE* test = fopen(name, "r");

	if (test != NULL){
		fclose(test);
		return true;
	}
	else {
		return false;
	}
}
