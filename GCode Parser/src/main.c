#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <Windows.h>

#include "FileTools.h"
#include "ParserTools.h"
#include "GCodeTools.h"

void init();

typedef struct layers{
	int start;
	int end;
	int skip;
	int full;
}layers;

int m_num_settings;
double m_ext_position;

int main() {
	init();

	FILE* in = openfile("r");

	findMaxSeek(in);

	FILE* out = initOutFile();

	copyHeader(in, out);

	int l_num, lightness, s_num;

	printf("How many sections to color? ");
	scanf("%d", &s_num);

	layers *sections = (layers*)malloc(s_num*(sizeof(layers)));
	
	for (int i = 0; i < s_num; i++){
		printf("Section %d:\n", i);
		printf("Start Layer? ");
		scanf("%d", &sections[i].start);
		printf("End Layer? ");
		scanf("%d", &sections[i].end);
		printf("Layer Skip? ");
		scanf("%d", &sections[i].skip);
		printf("Full Layer (1) or Perimeter (0)? ");
		scanf("%d", &sections[i].full);

	}

	l_num = 0;
	int s_head = 0;

	while (copyLayer(in, out, l_num) == 1){
		if (l_num >= sections[s_head].start && l_num <= sections[s_head].end && !(l_num % sections[s_head].skip)){
			if (sections[s_head].full){
				colorLayer(in, out, l_num);
			}
			else {
				colorPerim(in, out, l_num);
			}
		}

		if (l_num == sections[s_head].end && s_head < s_num){
			s_head++;
		}

		printf("Parsing layer %d\n", l_num++);
	}

	if (l_num == sections[s_head].end){
		if (sections[s_head].full){
			colorLayer(in, out, l_num);
		}
		else {
			colorPerim(in, out, l_num);
		}
	}
	
	fclose(in);
	fclose(out);

	system("PAUSE");
	return 0;
}

void init() {
	loadSettings();

	system("CLS");

	showMenu();

	setSlicer();

	setLayerDelay();
}
