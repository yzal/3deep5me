#ifndef FILETOOLS_H
#define FILETOOLS_H

#define _CRT_SECURE_NO_WARNINGS
#define INVALID_CHARS(a) ( (a) == '>' || (a) == '<' || (a) ==':' || (a) =='/' || (a) == 92 /*Backslash*/ || (a) =='|' || a=='?' || a=='*' || a <= 31)

#include <stdio.h>
#include <stdbool.h>
#include <windows.h>

FILE* openfile(char* mode);

FILE* initOutFile(void);

bool isLegalName(char *name);
bool fileExists(char *name);

#endif //FILETOOLS_H
