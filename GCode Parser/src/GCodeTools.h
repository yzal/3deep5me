#ifndef GCODETOOLS_H
#define GCODETOOLS_H

#define _CRT_SECURE_NO_WARNINGS

#define SETTINGS "options.ini"
#define NUM_SETTINGS 5

#define CURA 1
#define CRAFTWARE 2

#define RETRACT 1.000
#define RETRACT_FEED 4800


#include <stdio.h>
#include <stdbool.h>

#include "ParserTools.h"

typedef struct printhead {
	double x;
	double y;
	int servo;
	int servo_on;
	int servo_off;
} printhead;

// Slowly realizing I should be doing this in C++
printhead sharpie;
int slicer;

char layer[50];
char perim[50];
char last_line[50];

int l_seekback;
int e_seekback;

int wait;

long long int max_seek;

void loadSettings(void);
void getSettings(void);
void showMenu(void);
void setSlicer(void);
void setLayerDelay(void);
void findMaxSeek(FILE* in);

void injectString(FILE* out, char* string);
void injectPenState(FILE* out, bool enable, int wait);

void copyHeader(FILE* in, FILE* out);
int copyLayer(FILE* in, FILE* out, int l_num);
void engagePen(FILE* in, FILE* out, int l_num, char* begin, char* end);

void colorPerim(FILE* in, FILE* out, int l_num);
void colorLayer(FILE* in, FILE* out, int l_num);


void formatLayer(char* buffer, int l_num);



//char* getPerim(void){return perim;}
//char* getLayer(void){return layer;}

#endif //#define GCODETOOLS_H
