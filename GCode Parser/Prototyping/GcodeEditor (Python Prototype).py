

with open("3DEEP.gcode") as file:
    lines = file.readlines()
lines = [x.strip() for x in lines] 

header = lines[:24]
lines = lines[24:-22]
end = lines[-22:]

lineNum=0
zhop = 0
z = 0.3
layerHeight = 0.2
dz = layerHeight
block = []
result = []
layer = 1

for line in lines:
    lineNum = lines.index(line)
    if line[0] is ";" and line[1] is "L" and line[2] is "A":
        result.extend(block)
        block = []
        layer = int(line[7:]) +1
    if line[0] is ";" and line[1] is "T" and line[2] is "I": 
        result.append("G0 Z"+str(round(z+ dz*(layer),6)))
        
        lineNum = lines.index(line) + 2
    if line[0] is "G" and line[1] is "0":
        result.append("G0 Z"+str(round(z+ dz*(layer),6)))
        zhop=1
    else:
        if line[2] is "0":
            continue
        chunks = line.split()
        string = ""
        for chunk in chunks:
            if chunk[0] is "F" or chunk[0] is "E":
                continue
            if chunk[0] is "G" and chunk[1] is "0":
                    chunk = "G0 Z"+str(round(z+ dz*(layer),6))
            if chunk[0] is "X":
                   chunk = " X" + str(round(float(chunk[1:]) - 30.5 ,8))
            if chunk[0] is "Y":
                   chunk =  " Y" + str(round(float(chunk[1:]) + 15.5 ,8))
            if chunk[0] is "Z":
                   z = float(chunk[1:])
            string = (string + chunk)
            
        block.append(string + "      " + str(layer))
        if lines.index(line) is 1:
            block.append("G0 Z" + str(round(z+ dz*(layer-1),6)))
    result.append(line + "      " + str(layer))
    if (zhop==1):
        result.append("G0 Z"+str(round(z+ dz*(layer-1),6)))
        zhop = 0
        
    
        
with open("Dual3Deep.gcode","x") as file:
    for item in header + result + end:
        file.write("%s\n" % item)
    
